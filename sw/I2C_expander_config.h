/**
 * @file
 *
 * @brief Configuration file for I2C expander project
 */
#ifndef __I2C_EXPANDER_CONFIG_H_
#define __I2C_EXPANDER_CONFIG_H_
/* Includes --------------------------------------------------------------- */
#include <stdbool.h>

/* Defines ---------------------------------------------------------------- */
/**
 * @brief CPU clock speed in Hz
 *
 * Default is 1MHz (no crystal needed)
 */
#define F_CPU                   1000000

/**
 * @brief Expander I2C address
 */
#define I2C_EXPANDER_ADDR       64


/**
 * @brief Default pin mapping
 *
 * @{
 */
#define PIN00_PORT        D
#define PIN00_PIN         2

#define PIN01_PORT        D
#define PIN01_PIN         3

#define PIN02_PORT        D
#define PIN02_PIN         4

#define PIN03_PORT        B
#define PIN03_PIN         6

#define PIN04_PORT        B
#define PIN04_PIN         7

#define PIN05_PORT        D
#define PIN05_PIN         5

#define PIN06_PORT        D
#define PIN06_PIN         6

#define PIN07_PORT        D
#define PIN07_PIN         7
/* ------------------------ */
#define PIN08_PORT        C
#define PIN08_PIN         2

#define PIN09_PORT        C
#define PIN09_PIN         1

#define PIN10_PORT        C
#define PIN10_PIN         0

#define PIN11_PORT        B
#define PIN11_PIN         5

#define PIN12_PORT        B
#define PIN12_PIN         4

#define PIN13_PORT        B
#define PIN13_PIN         3

#define PIN14_PORT        B
#define PIN14_PIN         2

#define PIN15_PORT        B
#define PIN15_PIN         1

#define I2C_READY_ENABLED       true
#define I2C_READY_PORT          B
#define I2C_READY_PIN           0
/**
 * @}
 */


/**
 * @brief Enable/disable UART debug
 * @note Enabling debug traces might influence performance of expander and
 *       it can cause that some requests will not be accepted if they will
 *       be sent with short interval. Simply device will be printing message
 *       while another request is coming over I2C -> problem.
 */
#define I2C_UART_DEBUG          true

/* __I2C_EXPANDER_CONFIG_H_ */
#endif
