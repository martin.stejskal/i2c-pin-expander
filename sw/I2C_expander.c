/**
 * @file
 *
 * @brief Main file for project
 */
/* Includes --------------------------------------------------------------- */
#include "i2c_slave.h"
#include "uart_debug.h"
#include "pin_mapper.h"

#include "I2C_expander.h"
#include "I2C_expander_config.h"

/**
 * \brief Main function.
 *
 * @return If return then something is definitely wrong
 */
int
main (void)
{
  t_I2C_status eErrCode;
#if I2C_UART_DEBUG
  // Initialize debug UART first - can be used for debug
  UART_init ();

  // Show initialization message
  UART_PrintFlash(sMSG_INIT);
#endif
  I2C_init ();

  // Set all other pins as output
  set_all_io_as_output ();

  uint8_t au8data[2] =
    { 0x00 };

  // Main routine
  while (1)
    {
      eErrCode = I2C_rx_data (&au8data[0], 2);
      if (eErrCode == I2C_OK)
        {
#if I2C_UART_DEBUG
          UART_Print ("Setting pins: ");
          UART_PrintHEX (au8data[0], 0);
          UART_Print (" ; ");
          UART_PrintHEX (au8data[1], 1);
#endif
          pmap_set ((au8data[0] << 8) | (au8data[1] << 0));
        }
      else
        {
#if I2C_UART_DEBUG
          UART_Print ("E: ");
          UART_PrintHEX (eErrCode, 1);
#endif
          // Re-initialize I2C driver
          I2C_init();
        }
    }
  return (0);
}
