/**
 * @file
 *
 * @brief Driver for I2C module
 *
 * Big thanks to
 * http://www.tajned.cz/2016/11/twi-u-avr-2-dil-prakticke-priklady-ii/
 */
/* Includes --------------------------------------------------------------- */
#include <avr/io.h>
#include <util/twi.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>       /* For work with memory. There is used for save
                                 * text to flash, because these texts are not
                                 * changing.
                                 */

#include "i2c_slave.h"
#include "bit_operations.h"

#include "i2c_slave_config.h"

#if I2C_DEBUG_MSG
/* Messages --------------------------------------------------------------- */
const char sMSG_I2C_INTI_DONE[] PROGMEM = MSG_I2C_INTI_DONE;
const char sMSG_I2C_NACK[] PROGMEM = MSG_I2C_NACK;
const char sMSG_I2C_ERROR[] PROGMEM = MSG_I2C_ERROR;
const char sMSG_I2C_STOP_NDET[] PROGMEM = MSG_I2C_STOP_NDET;
const char sMSG_I2C_TIMEO_DATA[] PROGMEM = MSG_I2C_TIMEO_DATA;
const char sMSG_I2C_TIMEO_STOP[] PROGMEM = MSG_I2C_TIMEO_STOP;
const char sMSG_I2C_MISSING_DATA[] PROGMEM = MSG_I2C_MISSING_DATA;
#endif

/* Functions -------------------------------------------------------------- */
/**
 * @brief Initialize I2C module on device
 * @return I2C_OK if there is no error. Otherwise error code is returned.
 */
t_I2C_status
I2C_init (void)
{
#if I2C_SIGNALIZE_READY
  io_set_0(I2C_SIGNALIZE_READY_PORT, I2C_SIGNALIZE_READY_PIN);
#endif
  // Turn off I2C module while configuring
  TWCR &= ~(1 << TWEN);

  // For receiver, no prescaller should be set
  TWBR = 0;

  // Set slave address. Do not respond to global address (0x00)
  TWAR = (0 << TWGCE) | (I2C_EXPANDER_ADDR << TWA0);

  // Setup control register (datasheet - pg. 184)
  TWCR = (1 << TWEA) | (0 << TWSTA) | (0 << TWSTO) | (1 << TWEN);

#if I2C_SIGNALIZE_READY
  io_set_1(I2C_SIGNALIZE_READY_PORT, I2C_SIGNALIZE_READY_PIN);
#endif
  I2C_PRINT_FLASH(sMSG_I2C_INTI_DONE);

  return I2C_OK;
}

/**
 * @brief Blocking function for reading data from bus
 * @param pu8Data Pointer to buffer
 * @param u8NumOfBytes Define number of Bytes which should be read
 * @return I2C_OK if there is no error. Otherwise error code is returned.
 */
t_I2C_status
I2C_rx_data (uint8_t *pu8Data, uint8_t u8NumOfBytes)
{
  t_I2C_status eErrCode = I2C_ERROR;
  // "Timer"
  uint16_t u16Timer = 0;

  // Wait for some data. Well for address
  while (TW_STATUS != TW_SR_SLA_ACK)
    {
    }

#if I2C_SIGNALIZE_READY
  io_set_0(I2C_SIGNALIZE_READY_PORT, I2C_SIGNALIZE_READY_PIN);
#endif

  // React for change
  TWCR = (1 << TWEA) | (1 << TWINT) | (0 << TWSTA) | (0 << TWSTO) | (1 << TWEN);

  for (uint8_t u8Cnt = 0; u8Cnt < u8NumOfBytes; u8Cnt++)
    {
      // Wait for any data/whatever
      while (!((1 << TWINT) & TWCR))
        {
          if (u16Timer++ > I2C_COMMUNICATION_TIMEOUT)
            {
              I2C_PRINT_FLASH(sMSG_I2C_TIMEO_DATA);
              return I2C_TIMEOUT;
            }
        }
      if (TW_STATUS != TW_SR_DATA_ACK)
        {
          if (TW_STATUS == TW_SR_DATA_NACK)
            {
              eErrCode = I2C_DATA_NACK;
              I2C_PRINT_FLASH(sMSG_I2C_NACK);
            }
          else if (TW_STATUS == TW_SR_STOP)
            {
              eErrCode = I2C_MISSING_DATA;
              I2C_PRINT_FLASH(sMSG_I2C_MISSING_DATA);
            }
          else
            {
              eErrCode = I2C_ERROR;
              I2C_PRINT_FLASH(sMSG_I2C_ERROR);
              I2C_PRINT_HEX(TW_STATUS, 1);
            }
          TWCR = (1 << TWEA) | (1 << TWINT) | (0 << TWSTA) | (0 << TWSTO)
              | (1 << TWEN);

          return eErrCode;
        }
      // /if data not ACKed
      // Else read data from register and continue
      *(pu8Data++) = TWDR;
      TWCR = (1 << TWEA) | (1 << TWINT) | (0 << TWSTA) | (0 << TWSTO)
          | (1 << TWEN);
    }

  // Wait for stop
  while (!((1 << TWINT) & TWCR))
    {
      if (u16Timer++ > I2C_COMMUNICATION_TIMEOUT)
        {
          I2C_PRINT_FLASH(sMSG_I2C_TIMEO_STOP);
          return I2C_TIMEOUT;
        }
    }
  if (TW_STATUS != TW_SR_STOP)
    {
      I2C_PRINT_FLASH(MSG_I2C_STOP_NDET);
      I2C_PRINT_HEX(TW_STATUS, 1);

      TWCR = (1 << TWEA) | (1 << TWINT) | (0 << TWSTA) | (0 << TWSTO)
          | (1 << TWEN);

      return I2C_NO_STOP_DETECTED;
    }

  TWCR = (1 << TWEA) | (1 << TWINT) | (0 << TWSTA) | (0 << TWSTO) | (1 << TWEN);

#if I2C_SIGNALIZE_READY
  io_set_1(I2C_SIGNALIZE_READY_PORT, I2C_SIGNALIZE_READY_PIN);
#endif

  return (I2C_OK);
}

