/**
 * @file
 *
 * @brief Simple driver for sending strings via UART
 */
/* Includes --------------------------------------------------------------- */
#include "uart_debug.h"
#include "bit_operations.h"
#include <avr/io.h>

/* For work with memory. There is used for save text to flash, because these
 * texts are not changing.
 */
#include <avr/pgmspace.h>

/* Internal functions ----------------------------------------------------- */
void
_UART_SendByte (uint8_t u8Byte);

char
_UART_4bitsToASCII (uint8_t u8FourBits);

/* Functions -------------------------------------------------------------- */
/**
 * @brief Initialize UART HW
 */
void
UART_init (void)
{
  // Double speed if needed
  UCSRA = (UART_DOUBLE_SPEED << U2X);

  // Enable just transmitter - 8 bits
  UCSRB = (1 << TXEN) | (0 << UCSZ2);

  // Select URSRC, asynchronous, parity disabled, 1 stop bit, 8 bits
  UCSRC = (1 << URSEL) | (0 << UMSEL) | (0 << UPM1) | (0 << UPM0) | (0 << USBS)
      | (1 << UCSZ1) | (1 << UCSZ0);

  // Baud rate
  UBRRH = (0 << URSEL) | (0x0F & (UART_BRR >> 8));
  UBRRL = (UART_BRR & 0xFF);
}

/**
 * @brief Send data from flash memory
 * @param pFlashStr Pointer to flash where string is located
 */
void
UART_PrintFlash (const char * pFlashStr)
{
  // Now write Byte after Byte. Stop when NULL is found
  while ( pgm_read_byte(pFlashStr) != 0x00)
    {
      _UART_SendByte (pgm_read_byte(pFlashStr++));
    }
}

/**
 * @brief Send data from RAM
 *
 * Developer should avoid this function as much as possible, since it is
 * consuming RAM quite fast when using string. But for quick prototyping
 * it is suitable.
 *
 * @param pRAMStr Pointer to string stored in RAM
 */
void
UART_Print (const char * pRAMStr)
{
  while (*pRAMStr != 0x00)
    {
      _UART_SendByte (*pRAMStr++);
    }
}

/**
 * @brief Print Byte as HEX value
 * @param u8Value Byte which will be converted to ASCII
 * @param bAddNewLine Add new line? True/False
 */
void
UART_PrintHEX (uint8_t u8Value, uint8_t bAddNewLine)
{
  UART_Print ("0x");
  _UART_SendByte (_UART_4bitsToASCII ((u8Value >> 4) & 0x0F));
  _UART_SendByte (_UART_4bitsToASCII ((u8Value >> 0) & 0x0F));
  if (bAddNewLine)
    {
      _UART_SendByte ('\n');
    }
}

/* Internal functions ----------------------------------------------------- */
/**
 * @brief Blocking function that send only 1 Byte over UART
 * @param u8Byte Byte to be sent
 */
inline void
_UART_SendByte (uint8_t u8Byte)
{
  UDR = u8Byte;

  // Wait for transfer complete
  while (!(UCSRA & (1 << TXC)))
    ;
  // Wait for empty UDR
  while (!(UCSRA & (1 << UDRE)))
    ;
}

/**
 * @brief Convert 4 bit value to character
 *
 * If value bigger than 4 bits, the question mark will be returned
 * @param u8FourBits Input data
 * @return ASCII character
 */
char
_UART_4bitsToASCII (uint8_t u8FourBits)
{
  switch (u8FourBits)
    {
    case 0:
      return ('0');
    case 1:
      return ('1');
    case 2:
      return ('2');
    case 3:
      return ('3');
    case 4:
      return ('4');
    case 5:
      return ('5');
    case 6:
      return ('6');
    case 7:
      return ('7');
    case 8:
      return ('8');
    case 9:
      return ('9');
    case 10:
      return ('A');
    case 11:
      return ('B');
    case 12:
      return ('C');
    case 13:
      return ('D');
    case 14:
      return ('E');
    case 15:
      return ('F');
    default:
      return ('?');
    }
}
