/**
 * @file
 *
 * @brief Configuration file for I2C driver
 */
#ifndef __I2C_SLAVE_CONFGIG_H_
#define __I2C_SLAVE_CONFGIG_H_
/* Includes --------------------------------------------------------------- */
#include <stdbool.h>
#include "I2C_expander_config.h"
#include "uart_debug.h"

/* Settings --------------------------------------------------------------- */
/**
 * @brief Address of this device
 */
#define I2C_ADDR                        I2C_EXPANDER_ADDR

/**
 * @brief Enable/disable debug messages
 */
#define I2C_DEBUG_MSG                   I2C_UART_DEBUG

/**
 * @brief Enable signalizing "ready" by setting predefined output
 *
 * @{
 */
#define I2C_SIGNALIZE_READY             I2C_READY_ENABLED

#define I2C_SIGNALIZE_READY_PORT        I2C_READY_PORT
#define I2C_SIGNALIZE_READY_PIN         I2C_READY_PIN
/**
 * @}
 */

/**
 * @brief Maximum number of wait cycles when waiting for data
 *
 * When address is received, program waiting for other CLK + data. To avoid
 * system freeze/unexpected states it is possible to timeout and report
 * error. Higher layers should re-initialize I2C driver or take appropriate
 * action. Value is in "while cycles"
 */
#define I2C_COMMUNICATION_TIMEOUT       10000

/**
 * @brief Mapping for "print" functions
 *
 * Most probably you want to call functions that "print" text over UART
 *
 * @{
 */
#if I2C_DEBUG_MSG
#define I2C_PRINT_FLASH(str)            UART_PrintFlash(str)
#define I2C_PRINT_RAM(str)              UART_Print(str)
#define I2C_PRINT_HEX(str, nl)          UART_PrintHEX(str, nl)
#else
#define I2C_PRINT_FLASH(str)
#define I2C_PRINT_RAM(str)
#define I2C_PRINT_HEX(str, nl)
#endif
/**
 * @}
 */
/* __I2C_SLAVE_CONFGIG_H_ */
#endif
