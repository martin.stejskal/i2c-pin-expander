/**
 * \file
 *
 * \author Martin Stejskal
 *
 * \brief Macros and functions for bit operations in C
 *
 * Some operations are not "easy to write" so there is "easy to use". Most\n
 * are common for many platforms, but some can be only for 8bit AVR\n
 * (operations with I/O pins). Anyway this library can be used cross many\n
 * platforms when basic operations are used.
 *
 * Modified: 19.06.2014
 * Created: 2013
 *
 * \version 0.2.2
 *
*/
#include "bit_operations.h"

