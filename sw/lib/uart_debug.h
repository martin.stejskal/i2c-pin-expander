/**
 * @file
 *
 * @brief Simple driver for sending strings via UART
 */
#ifndef __UART_DEBUG_H_
#define __UART_DEBUG_H_
/* Includes --------------------------------------------------------------- */
#include "uart_debug_config.h"
#include <avr/pgmspace.h>

void
UART_init (void);

void
UART_PrintFlash (const char * pFlashStr);

void
UART_Print (const char * pRAMStr);

void
UART_PrintHEX (uint8_t u8Value, uint8_t bAddNewLine);

/* __UART_DEBUG_H_ */
#endif
