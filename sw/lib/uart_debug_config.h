/**
 * @file
 *
 * @brief Configuration for UART debug output
 */
#ifndef __UART_DEBUG_CONFIG_H_
#define __UART_DEBUG_CONFIG_H_
/* Includes --------------------------------------------------------------- */
#include "i2c_slave_config.h"

/* Defines ---------------------------------------------------------------- */
/**
 * @brief UART baudrate parameters
 *
 * Normal mode:
 * UBRR = (Fosc/16*BAUD) -1
 *
 * Double speed mode:
 * UBRR = (Fosc/8*BAUD) -1
 *
 * @{
 */
#define UART_BRR                0
#define UART_DOUBLE_SPEED       1
/**
 * @}
 */

/* __UART_DEBUG_CONFIG_H_ */
#endif
