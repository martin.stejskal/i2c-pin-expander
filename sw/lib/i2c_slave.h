/**
 * @file
 *
 * @brief Driver for I2C module
 */
#ifndef __I2C_SLAVE_H__
#define __I2C_SLAVE_H__
/* Includes --------------------------------------------------------------- */
#include <inttypes.h>           // Define integer types

/* Defines ---------------------------------------------------------------- */
/**
 * @brief Messages which will be printed to UART if something goes wrong
 *
 * @{
 */
#define MSG_I2C_PFX             "I2C slave: "
#define MSG_I2C_INTI_DONE       MSG_I2C_PFX "Initialization done\n"
#define MSG_I2C_NACK            MSG_I2C_PFX "data NACK\n"
#define MSG_I2C_ERROR           MSG_I2C_PFX "error "
#define MSG_I2C_STOP_NDET       MSG_I2C_PFX "stop bit not detected "
#define MSG_I2C_TIMEO_DATA      MSG_I2C_PFX "timeout when waiting for data\n"
#define MSG_I2C_TIMEO_STOP      MSG_I2C_PFX \
                                "timeout when waiting for stop bit\n"
#define MSG_I2C_MISSING_DATA    MSG_I2C_PFX "premature stop bit detected. "\
                                "Expected data Bytes\n"
/**
 * @}
 */
/* Enums, structures ------------------------------------------------------ */
/**
 * @brief Error codes for I2C driver
 */
typedef enum
{
  I2C_OK = 0,          //!< I2C_OK No error
  I2C_ERROR = 1,       //!< I2C_ERROR Generic error
  I2C_TIMEOUT,         //!< I2C_TIMEOUT Timeout error
  I2C_DATA_NACK,       //!< I2C_DATA_NACK Data NACked
  I2C_NO_STOP_DETECTED,//!< I2C_NO_STOP_DETECTED Stop bit not detected
  I2C_MISSING_DATA,    //!< I2C_MISSING_DATA Missing data. Stop detected sooner
} t_I2C_status;

/* Functions -------------------------------------------------------------- */
t_I2C_status
I2C_init (void);

t_I2C_status
I2C_rx_data (uint8_t *pu8Data, uint8_t u8NumOfBytes);

#endif /* __I2C_SLAVE_H__ */
