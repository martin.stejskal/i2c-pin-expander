/**
 * @file
 *
 * @brief Library that handle pin mapping
 */
/* Includes --------------------------------------------------------------- */
#include "pin_mapper.h"
#include "pin_mapper_config.h"
#include "bit_operations.h"
/* Functions -------------------------------------------------------------- */
/**
 * @brief Initialize pins
 *
 * This have to be called before @ref pmap_set function in order to set used
 * pins to output.
 */
void
pmap_init (void)
{
  io_set_dir_out(P00_PORT, P00_PIN);
  io_set_dir_out(P01_PORT, P01_PIN);
  io_set_dir_out(P02_PORT, P02_PIN);
  io_set_dir_out(P03_PORT, P03_PIN);
  io_set_dir_out(P04_PORT, P04_PIN);
  io_set_dir_out(P05_PORT, P05_PIN);
  io_set_dir_out(P06_PORT, P06_PIN);
  io_set_dir_out(P07_PORT, P07_PIN);

  io_set_dir_out(P08_PORT, P08_PIN);
  io_set_dir_out(P09_PORT, P09_PIN);
  io_set_dir_out(P10_PORT, P10_PIN);
  io_set_dir_out(P11_PORT, P11_PIN);
  io_set_dir_out(P12_PORT, P12_PIN);
  io_set_dir_out(P13_PORT, P13_PIN);
  io_set_dir_out(P14_PORT, P14_PIN);
  io_set_dir_out(P15_PORT, P15_PIN);
}

/**
 * @brief Set pins
 * @param u16pins Bits in variable define physical pins. For example LSB is
 *        mapped to P00_PORT, P00_PIN. If zero, pin will be set to 0, otherwise
 *        it is set to 1.
 */
void
pmap_set (uint16_t u16pins)
{
  if (is_bit_set(u16pins, 0))
    io_set_1(P00_PORT, P00_PIN);
  else
    io_set_0(P00_PORT, P00_PIN);
  if (is_bit_set(u16pins, 1))
    io_set_1(P01_PORT, P01_PIN);
  else
    io_set_0(P01_PORT, P01_PIN);
  if (is_bit_set(u16pins, 2))
    io_set_1(P02_PORT, P02_PIN);
  else
    io_set_0(P02_PORT, P02_PIN);
  if (is_bit_set(u16pins, 3))
    io_set_1(P03_PORT, P03_PIN);
  else
    io_set_0(P03_PORT, P03_PIN);
  if (is_bit_set(u16pins, 4))
    io_set_1(P04_PORT, P04_PIN);
  else
    io_set_0(P04_PORT, P04_PIN);
  if (is_bit_set(u16pins, 5))
    io_set_1(P05_PORT, P05_PIN);
  else
    io_set_0(P05_PORT, P05_PIN);
  if (is_bit_set(u16pins, 6))
    io_set_1(P06_PORT, P06_PIN);
  else
    io_set_0(P06_PORT, P06_PIN);
  if (is_bit_set(u16pins, 7))
    io_set_1(P07_PORT, P07_PIN);
  else
    io_set_0(P07_PORT, P07_PIN);
  if (is_bit_set(u16pins, 8))
    io_set_1(P08_PORT, P08_PIN);
  else
    io_set_0(P08_PORT, P08_PIN);
  /* ------------------------ */
  if (is_bit_set(u16pins, 9))
    io_set_1(P09_PORT, P09_PIN);
  else
    io_set_0(P09_PORT, P09_PIN);
  if (is_bit_set(u16pins, 10))
    io_set_1(P10_PORT, P10_PIN);
  else
    io_set_0(P10_PORT, P10_PIN);
  if (is_bit_set(u16pins, 11))
    io_set_1(P11_PORT, P11_PIN);
  else
    io_set_0(P11_PORT, P11_PIN);
  if (is_bit_set(u16pins, 12))
    io_set_1(P12_PORT, P12_PIN);
  else
    io_set_0(P12_PORT, P12_PIN);
  if (is_bit_set(u16pins, 13))
    io_set_1(P13_PORT, P13_PIN);
  else
    io_set_0(P13_PORT, P13_PIN);
  if (is_bit_set(u16pins, 14))
    io_set_1(P14_PORT, P14_PIN);
  else
    io_set_0(P14_PORT, P14_PIN);
  if (is_bit_set(u16pins, 15))
    io_set_1(P15_PORT, P15_PIN);
  else
    io_set_0(P15_PORT, P15_PIN);
}
