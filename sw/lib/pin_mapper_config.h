/**
 * @file
 *
 * @brief Configuration file for pin mapper
 */
#ifndef __PIN_MAPPER_CONFIG_H_
#define __PIN_MAPPER_CONFIG_H_
/* Includes --------------------------------------------------------------- */
#include "I2C_expander_config.h"
/* Defines ---------------------------------------------------------------- */
/**
 * @brief Default pin mapping
 *
 * @{
 */
#define P00_PORT        PIN00_PORT
#define P00_PIN         PIN00_PIN

#define P01_PORT        PIN01_PORT
#define P01_PIN         PIN01_PIN

#define P02_PORT        PIN02_PORT
#define P02_PIN         PIN02_PIN

#define P03_PORT        PIN03_PORT
#define P03_PIN         PIN03_PIN

#define P04_PORT        PIN04_PORT
#define P04_PIN         PIN04_PIN

#define P05_PORT        PIN05_PORT
#define P05_PIN         PIN05_PIN

#define P06_PORT        PIN06_PORT
#define P06_PIN         PIN06_PIN

#define P07_PORT        PIN07_PORT
#define P07_PIN         PIN07_PIN
/*---------------------*/
#define P08_PORT        PIN08_PORT
#define P08_PIN         PIN08_PIN

#define P09_PORT        PIN09_PORT
#define P09_PIN         PIN09_PIN

#define P10_PORT        PIN10_PORT
#define P10_PIN         PIN10_PIN

#define P11_PORT        PIN11_PORT
#define P11_PIN         PIN11_PIN

#define P12_PORT        PIN12_PORT
#define P12_PIN         PIN12_PIN

#define P13_PORT        PIN13_PORT
#define P13_PIN         PIN13_PIN

#define P14_PORT        PIN14_PORT
#define P14_PIN         PIN14_PIN

#define P15_PORT        PIN15_PORT
#define P15_PIN         PIN15_PIN

/**
 * @}
 */

/* __PIN_MAPPER_H_ */
#endif
