/**
 * @file
 *
 * @brief Additional macros, definitions for I2C expander
 */
#ifndef __I2C_EXPANDER_H_
#define __I2C_EXPANDER_H_
/* Includes --------------------------------------------------------------- */
#include <avr/io.h>
#include <avr/pgmspace.h>

/* Defines ---------------------------------------------------------------- */
/**
 * @brief Software version
 */
#define I2C_EXPANDER_VERSION    "1.1"
/* Messages --------------------------------------------------------------- */
/**
 * @brief Initial description which is used to identify device
 */
const char sMSG_INIT[] PROGMEM = "\n\n\n\n== I2C expander v "
                                 I2C_EXPANDER_VERSION " ==\n";

/* Functions -------------------------------------------------------------- */
/**
 * @brief Set all I/O pins as output
 *
 * I
 */
void
set_all_io_as_output ()
{
#ifdef DDRA
  DDRA = ~0;
#endif
#ifdef DDRB
  DDRB = ~0;
#endif
#ifdef DDRC
  DDRC = ~0;
#endif
#ifdef DDRD
  DDRD = ~0;
#endif
#ifdef DDRE
  DDRE = ~0;
#endif
#ifdef DDRF
  DDRF = ~0;
#endif
#ifdef DDRG
  DDRG = ~0;
#endif
#ifdef DDRH
  DDRH = ~0;
#endif
}

/* __I2C_EXPANDER_H_ */
#endif
