/**
 * @file
 *
 * @brief Library that handle pin mapping
 */
#ifndef __PIN_MAPPER_H_
#define __PIN_MAPPER_H_
/* Includes --------------------------------------------------------------- */
#include <stdint.h>
/* Functions -------------------------------------------------------------- */
void
pmap_init (void);

void
pmap_set (uint16_t u16pins);

/* __PIN_MAPPER_H_ */
#endif
