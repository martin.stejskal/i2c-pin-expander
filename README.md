# About
 * This device receive data via I2C/TWI bus and "put" these data to I/O.
 * Written for ATmega8.

# How to use
 * It is expected that you have AVR toolchain installed and in PATH.
 * User configuration is at `I2C_expander_config.h`

## Flash ATmega8
 * Assume that you're using [USBasp](https://www.fischl.de/usbasp/) programmer.
   If not, change `programmer` in makefile.

```bash
cd sw && make program_hex
```

## Send data via I2C
 * Device address is defined at `I2C_expander_config.h` and by default it is
   `0x40`.
 * Default pin mapping is also at `I2C_expander_config.h`.
 * Now just send 3 Bytes including address and write request. Example:

```
Start 0x80 0x01 0x02 Stop
```

 * This will set pins `PC2` and `PD2` to `1`. Other pins will be set to `0`.
 * By default is enabled UART for debugging -> send data only when UART stop
   sending any debug messages. Busy is also signalized by high level on PB0
   by default.

